#!/usr/bin/env python
from sys import argv, exit, stderr
import gzip
import re

def print_usage():
    print("Usage:")
    print("./extract-rsid.py --outputtype input.vcf.gz 1000Genomes.ped target_population marker_file > output.dat")
    print("For example:")
    print("./extract-rsid.py --genotypename {0} {1} {2} {3} > {3}-{2}.dat".format(
        "ALL.chr19.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz",
        "integrated_call_samples_v2.20130502.ALL.ped", "YRI", "chr19_marker_file_YRI.dat"
    ))
    print("")
    print("When using output type --indivname, marker_file is not needed")
    print("")
    print("The script expects gzipped VCF files from the 1000Genomes project:")
    print("ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/phase1/analysis_results/integrated_call_sets/")

def get_output_type(flag):
    if not flag.startswith("--"):
        raise Exception("Please provide output type")
    return flag[2:]

def parse_marker_file(filename):
    with open(filename) as handle:
        for line in handle:
            yield line.split()[0]

def parse_input(filename, rsIDs):
    header, data = None, []
    if rsIDs:
        pattern = lambda rs: re.compile(r'|'.join(r+"\s" for r in rs))
    else:
        pattern = lambda rs: re.compile(r'.*')
    with gzip.open(argv[2]) as handle:
        for line in handle:
            if line.startswith("#CHROM"):
                header = line.split()
            if (not line.startswith("#")) and pattern(rsIDs).search(line):
                fields = line.split()
                try:
                    i = rsIDs.index(fields[2])
                    del rsIDs[i]
                    stderr.write("{} rsIDs left\r".format(len(rsIDs)))
                except ValueError:
                    pass
                data.append(line.split())
                if not rsIDs:
                    stderr.write("\n")
                    break
    return header, data

def get_target_ids(population_file, population):
    with open(population_file) as handle:
        for i, line in enumerate(handle):
            if i > 0:
                fields = line.split()
                if fields[6] == population:
                    yield fields[1]

def get_genders(population_file):
    with open(population_file) as handle:
        for line in handle:
            fields = line.split()
            yield fields[1], {"1": "M", "2": "F"}.get(fields[4], "?")

def extract_raw(header, data):
    if header and data:
        individuals = header[9:]
        for data_line in data:
            variants = [data_line[3]] + data_line[4].split(",")
            calls = data_line[9:]
            rsID = data_line[2]
            yield individuals, rsID, variants, calls
    else:
        raise IOError("Input file missing VCF header and/or entry for snip")

def numeric(call):
    return str(sum(int(c) for c in call.split("|")))

def generate_output(individuals, variants, calls, rsID, population_ids, population, genders, output_type):
    for individual, call in zip(individuals, calls):
        if individual in population_ids:
            if output_type == "genotypename":
                genotype = "|".join(variants[int(c)] for c in call.split("|"))
                gender = genders[individual]
                print("\t".join([rsID, individual, numeric(call)]))
            if output_type == "indivname":
                gender = genders[individual]
                print("\t".join([individual, gender, population]))

def main(argv):
    if len(argv) < 5:
        print_usage()
        exit()
    output_type = get_output_type(argv[1])
    if len(argv) < 6:
        rsIDs = []
    else:
        rsIDs = list(parse_marker_file(argv[5]))
    header, data = parse_input(argv[2], rsIDs)
    population_file, population = argv[3], argv[4]
    population_ids = list(get_target_ids(population_file, population))
    genders = dict(get_genders(population_file))
    for individuals, rsID, variants, calls in extract_raw(header, data):
        generate_output(individuals, variants, calls, rsID, population_ids, population, genders, output_type)

if __name__ == "__main__":
    exit(main(argv))
